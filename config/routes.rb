Rails.application.routes.draw do

  root 'information_pages#home'
  get '/help',      to: 'information_pages#help'
  get '/about',     to: 'information_pages#about'
  get '/contact',   to: 'information_pages#contact'
  get '/places',    to: 'places#index'
  get '/addplace',    to: 'places#new'
  post '/addplace',   to: 'places#create'

  resources :places
end
