
30.times do |n|
  name = Faker::Company.name
  description = Faker::Hipster.paragraph(15, true, 8)
  slug = Faker::Internet.slug(name)
  Place.create!(name: name,
                description: description,
                facebook: "http://www.facebook.com/#{slug}",
                instagram: "http://www.instagram.com/#{slug}")
end

description = "Casa Grande (literally \"Big House\") is located in Cempoala, Veracruz, México. Begun build about 1877 to 1880, being the owners Mr. Fermin Zarate and Mrs. Tomasa Licona de Zarate, parents of the world famous dwarf Manuel and Lucia Zarate (35 cm. In height according Ripley's in one of his famous cartoons \"Although you not believe it \").

For the construction of the house was necessary to hire a Japanese company to talara trees at that time the buildings were on archaeological woods for them (some now extinct) are used for construction of the estate. Recall that the Zarate family had bought the state government a portion of the estate \"of Lencero\". This 5.000 acre property was so extensive that Mr. Zarate bragged saying, \"I can climb the biggest tree is the tallest pyramid and all I could see and beyond is mine ...\"."

Place.create!(name: "Casa Grande (Museum)",
              description: description,
              facebook: "http://www.facebook.com/",
              instagram: "http://www.instagram.com/")

description = "Facebook is a corporation and an online social networking service headquartered in Menlo Park, California, in the United States. Its website was launched on February 4, 2004, by Mark Zuckerberg with his Harvard College roommates and fellow students Eduardo Saverin, Andrew McCollum, Dustin Moskovitz and Chris Hughes. The founders had initially limited the website's membership to Harvard students, but later expanded it to colleges in the Boston area, the Ivy League, and Stanford University. It gradually added support for students at various other universities and later to high-school students. Since 2006, anyone in general who has attained an age of 13 years or more has been allowed to become a registered user of the website, though variations in the minimum age requirement exist depending on applicable local laws. Its name comes from the face book directories often given to American university students."

Place.create!(name: "Facebook",
              description: description,
              facebook: "http://www.facebook.com/",
              instagram: "http://www.instagram.com/")

description = "Meliá Hotels International, S.A. (formerly Sol Meliá) is a Spanish hotel chain which was founded by Gabriel Escarrer Juliá in 1956 in Palma de Mallorca. It is also known as and referred to by its former name of Sol Meliá. The company is one of Spain's largest domestic operators of holiday resorts and the 17th biggest hotel chain worldwide. Domestically in Spain the company is the market leader in both resort and urban hotels. Currently the hotel chain operates 374 hotels in 40 countries on 4 continents under the brands Meliá, Gran Meliá, ME by Meliá, Paradisus, Innside by Meliá, TRYP by Wyndham, Sol Hotels and Club Meliá."

Place.create!(name: "Meliá Hotels International",
              description: description,
              facebook: "http://www.facebook.com/",
              instagram: "http://www.instagram.com/")

description = "Varadero is foremost a tourist resort town, boasting more than 20 km of white sandy beaches. The first tourists visited Varadero as early as the 1870s, and for years it was considered an elite resort. In 1910 the annual rowing regatta was started; five years later the first hotel, named Varadero and later Club Nautico, was built. Tourism grew in the early 1930s as Irénée du Pont, an American millionaire, built his estate on the peninsula. Many famous and infamous people stayed in Varadero, for example Al Capone."

Place.create!(name: "Varadero",
              description: description,
              facebook: "http://www.facebook.com/",
              instagram: "http://www.instagram.com/")

description = "The hotel was built as the Habana Hilton at a cost of $24 million under the personal auspices of President Fulgencio Batista, as an investment by the pension plan of the Cuban Catering Workers Union, and was operated by the American Hilton Hotels group. The hotel was designed by the well-known Los Angeles architect Welton Becket, who had previously designed the Beverly Hilton for the chain, in collaboration with the Havana-based architects Lin Arroyo and Gabriela Menéndez. Arroyo was the Minister of Public Works under Batista."

Place.create!(name: "Habana Libre",
              description: description,
              facebook: "http://www.facebook.com/",
              instagram: "http://www.instagram.com/")

description = "The University of Havana or UH (in Spanish, Universidad de La Habana) is a university located in the Vedado district of Havana, the capital of the Republic of Cuba. Founded on January 5, 1728, the university is the oldest in Cuba, and one of the first to be founded in the Americas. Originally a religious institution, today the University of Havana has 15 faculties (colleges) at its Havana campus and distance learning centers throughout Cuba."

Place.create!(name: "University of Havana",
              description: description,
              facebook: "http://www.facebook.com/",
              instagram: "http://www.instagram.com/")


# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
