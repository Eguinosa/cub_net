class CreatePlaces < ActiveRecord::Migration[5.0]
  def change
    create_table :places do |t|
      t.string :name
      t.text :description
      t.string :facebook
      t.string :instagram
      t.text :json

      t.timestamps
    end
    add_index :places, :name
  end
end
