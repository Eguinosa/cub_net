# CUbNEt APP

This is the CUbNEt application, the better place to gather all of your favorites places.

Made for William by Gelin Eguinosa as a test.

## License

In this very moment, you don't need any license to use this application, so enjoy all the features.

## Getting started

To get started with the app, clone the repo and then install the needed gems:

```
$ bundle install
```

Next, migrate the database:

```
$ rails db:migrate
```

Finally, run the test suite to verify that everything is working correctly:

```
$ rails test
```

If the test suite passes, you'll be ready to run the app in a local server:

```
$ rails server
```

### Ruby version

Ruby 2.3.1

### Rails version

Rails 5.0.0.1
